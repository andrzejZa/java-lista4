package com.az;

import com.az.Utils.DateHelper;
import com.az.models.Dzien;
import com.az.models.Firma;

import com.az.models.NieRobotnik;
import com.az.models.Robotnik;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Firma f = new Firma();
        f.DodajPracownika(new Robotnik("Kowalski1",0.5, DateHelper.CreateDate( 2018,0,2), 10));
        f.DodajPracownika(new NieRobotnik("Kowalski2",0.5, DateHelper.CreateDate( 2018,0,3), 0.5, 1500));
        f.DodajPracownika(new Robotnik("Kowalski3",0.5, DateHelper.CreateDate( 2018,0,4), 10));
        f.WygenerujGodziny();
        f.ObliczPracownikow();
    }
}
