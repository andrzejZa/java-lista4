package com.az.Utils;

import java.util.Calendar;
import java.util.Date;

public class DateHelper {
    public boolean DatesEqual(Date d1, Date d2){
        if (d1.getYear() != d2.getYear())
            return false;
        if (d1.getMonth() != d2.getMonth())
            return false;
        if (d1.getDay() != d2.getDay())
            return false;
        return true;

    }
    public static Date CreateDate(int year, int month, int day){
        Calendar c = Calendar.getInstance();
        c.set(year,month,day);
        return c.getTime();
    }
}
