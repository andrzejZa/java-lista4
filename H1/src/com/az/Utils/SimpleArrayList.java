package com.az.Utils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

public class SimpleArrayList<T> {

    private int length;
    private Object[] storage;
    private final int partLength = 100;

    public SimpleArrayList() {
        length = 0;
        storage = new Object[partLength];
    }

    private SimpleArrayList(Object[] array) {

        length = 0;
        storage = new Object[partLength];
        for (int i = 0; i < array.length; i++)
            this.Add((T) array[i]);
    }

    public T Get(int index) {
        return (T) storage[index];
    }

    public void Set(int index, T value) {
        storage[index] = value;
    }

    public int FirstIndexOf(T value) {
        boolean isComparable = (storage[0] instanceof Comparable);
        for (int i = 0; i < length; i++) {
            if (isComparable ? ((Comparable) storage[i]).compareTo(value) == 0 : storage[i] == value)
                return i;
        }

        return -1;

    }

    public int IndexOf(T value) {
        for (int i = 0; i < length; i++) {
            if (storage[i] == value)
                return i;
        }
        return -1;
    }

    public void RemoveAt(int index) {
        Object[] newStorage = new Object[storage.length];
        boolean itemRemoved = false;
        for (int i = 0; i < length; i++) {
            if (i != index) {
                newStorage[i] = storage[i];
            } else {
                itemRemoved = true;
            }
        }
        if (itemRemoved) {
            storage = newStorage;
            length--;
        }
    }

    public void RemoveAll(T elem) {
        boolean isComparable = (storage[0] instanceof Comparable);
        Object[] newStorage = new Object[storage.length];
        int itemRemoved = 0;

        for (int i = 0; i < length; i++) {
            if (!(isComparable ? (((Comparable) storage[i]).compareTo(elem) == 0) : (storage[i] == elem))) {
                newStorage[i] = storage[i];
            } else {
                itemRemoved++;
            }
        }
        if (itemRemoved > 0) {
            storage = newStorage;
            length -= itemRemoved;
        }
    }

    public T Max() {

        T max = (T) storage[0];
        if (!(storage[0] instanceof Comparable))
            return max;
        for (int i = 0; i < length; i++) {
            if (((Comparable) max).compareTo(storage[i]) < 0)
                max = (T) storage[i];
        }
        return max;
    }

    public T Min() {
        T min = (T) storage[0];
        if (!(storage[0] instanceof Comparable))
            return min;
        for (int i = 0; i < length; i++) {
            if (((Comparable) min).compareTo(storage[i]) > 0)
                min = (T) storage[i];
        }
        return min;
    }

    public SimpleArrayList<T> SortAsc() {
        return Sort(true);
    }

    public SimpleArrayList<T> SortDesc() {
        return Sort(false);
    }

    private SimpleArrayList<T> Sort(boolean asc) {
        Object[] resultArr = new Object[length];
        for (int i = 0; i < length; i++) {
            resultArr[i] = storage[i];
        }
        if (storage[0] instanceof Comparable) {
            T temp;


            for (int i = 0; i < length; i++) {
                for (int j = 1; j < (length - i); j++) {

                    if (asc ? ((Comparable) resultArr[j - 1]).compareTo(resultArr[j]) > 0 :
                            ((Comparable) resultArr[j - 1]).compareTo(resultArr[j]) < 0) {
                        //swap the elements!
                        temp = (T) resultArr[j - 1];
                        resultArr[j - 1] = resultArr[j];
                        resultArr[j] = temp;
                    }

                }
            }

        }
        return new SimpleArrayList<T>(resultArr);
    }

    public SimpleArrayList<T> Distinct() {
        SimpleArrayList<T> result = new SimpleArrayList();

        for (int i = 0; i < length; i++) {
            if (result.FirstIndexOf((T) storage[i]) == -1)
                result.Add((T) storage[i]);
        }
        return result;
    }

    public SimpleArrayList<T> NotUnique() {
        SimpleArrayList<T> list = new SimpleArrayList();
        SimpleArrayList<T> resultList = new SimpleArrayList();

        for (int i = 0; i < length; i++) {
            if (list.FirstIndexOf((T) storage[i]) == -1)
                list.Add((T) storage[i]);
            else resultList.Add((T) storage[i]);

        }
        return resultList;
    }

    public int size() {
        return length;
    }

    public <T> T[] ToArray() {
        if (length == 0)
            return null;
        @SuppressWarnings("unchecked")
        T[] tempStorage = (T[]) Array.newInstance(storage[0].getClass(), length);

        for (int i = 0; i < length; i++) {
            tempStorage[i] = (T) storage[i];
        }
        return tempStorage;
    }

    public void Add(T item) {
        if (length == storage.length) {
            Object[] newStorage = new Object[storage.length + partLength];
            for (int i = 0; i < storage.length; i++) {
                newStorage[i] = storage[i];
            }
            storage = newStorage;
        }

        storage[length] = item;
        length++;
    }


    public void AddSome(T[] items) {
        for (int i = 0; i < items.length; i++) {
            this.Add(items[i]);
        }
    }

    public T Find(String field, Object value) {
        try {
            Field prop = storage[0].getClass().getDeclaredField(field);
        } catch (NoSuchFieldException ex) {
            return null;
        }
        for (int i = 0; i < length; i++) {
            try {
                Field prop = storage[i].getClass().getDeclaredField(field);
                if (prop.equals(value))
                    return (T) storage[i];
            } catch (NoSuchFieldException ex) {
                continue;
            }


        }
        return null;
    }
}
