package com.az.models;

import java.util.Date;

public class Dzien{

    public int iloscGodzin;
    public Date date;

    public Dzien(int iloscGodzin, Date date) {
        this.iloscGodzin = iloscGodzin;
        this.date = date;
    }
}