package com.az.models;

import com.az.TableBuilder;
import com.az.Utils.DateHelper;
import com.az.Utils.SimpleArrayList;

import java.util.Calendar;
import java.util.Date;

public class Firma {
    SimpleArrayList<Pracownik> Pracownicy;

    public Firma() {
        Pracownicy = new SimpleArrayList<>();
    }

    Pracownik ZanjdzPracownika(String nazwisko){
        return Pracownicy.Find("Nazwisko",nazwisko);

    }

    boolean CzyJestNieRobotnikiem(Pracownik pracownik){
        return pracownik instanceof NieRobotnik;
    }

    public void DodajPracownika(Pracownik pracownik){
        Pracownicy.Add(pracownik);
    }

    void ZwolnijPracownika(Pracownik pracownik){
        Pracownicy.RemoveAll(pracownik);
    }

    void ObliczPracownika(String nazwisko){
        Pracownik pracownik = Pracownicy.Find("Nazwisko",nazwisko);
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        pracownik.ObliczMiesiacPracy(cal.get(Calendar.MONTH));
        TableBuilder tb = new TableBuilder();
        tb.AddHeader(new String[]{"Nazwisko","Typ","Miesiac","Suma"});
        tb.AddRow(pracownik.WydrukujMiesiacPracy(cal.get(Calendar.MONTH)));
        tb.Print();
    }

    public void ObliczPracownikow(){
        TableBuilder tb = new TableBuilder();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        tb.AddHeader(new String[]{"Nazwisko","Typ","Miesiac","Suma"});
        for (int i=0;i<Pracownicy.size();i++ ){
            tb.AddRow(Pracownicy.Get(i).WydrukujMiesiacPracy(cal.get(Calendar.MONTH)));
        }
        tb.Print();
    }

    //generuje godziny ostatniego przeszlego miesiaca
    public void WygenerujGodziny(){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.MONTH, -1);
        for (int i=0;i< c.getActualMaximum(Calendar.DAY_OF_MONTH);i++){
            for (int j=0;j< Pracownicy.size();j++){
                Date date = DateHelper.CreateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), i + 1);
                if (Pracownicy.Get(j).ZaczalPracowac.compareTo(date)<0 ) {
                    Pracownicy.Get(j).DniPracy.Add(
                            new Dzien((int) (Math.random() * 10),
                                    date));
                }
            }
        }

    }
}
