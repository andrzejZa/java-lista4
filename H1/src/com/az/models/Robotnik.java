package com.az.models;



import com.az.Utils.SimpleArrayList;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

abstract class Pracownik implements Comparable<Pracownik>{
     static final int EtatGodzin = 8;
    public String Nazwisko;
    public SimpleArrayList<Dzien> DniPracy;
    public double Etat;
    public Date ZaczalPracowac;
    public abstract double ObliczDzienPracy(Dzien dzien);

    public double ObliczDzienPracy(Date date){
        Date dzienPracy = date;
        for (int i=0;i< DniPracy.size();i++) {
            if (DniPracy.Get(i).date == dzienPracy)
                return ObliczDzienPracy(DniPracy.Get(i));
        }
        return 0;
    }

    public double ObliczMiesiacPracy (int miesiac) {
        double sum =0;
        for (int i=0;i< DniPracy.size();i++){
            if (DniPracy.Get(i).date.getMonth() == miesiac)
                sum += ObliczDzienPracy(DniPracy.Get(i));
        }
        return sum;
    }
    public String[] WydrukujMiesiacPracy(int miesiac){
        double sum = ObliczMiesiacPracy(miesiac);
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        return new String[]{Nazwisko,months[miesiac],this.getClass().getSimpleName(),sum+"" };

    }

    @Override
    public int compareTo(Pracownik o) {

        return Nazwisko.compareTo(o.Nazwisko);
    }

    public Pracownik(String nazwisko, double etat, Date zaczalPracowac) {
        Nazwisko = nazwisko;
        Etat = etat;
        ZaczalPracowac = zaczalPracowac;
        DniPracy = new SimpleArrayList<>();
    }
}

public class Robotnik extends Pracownik{
    double godzinowaStawka;
    @Override
    public double ObliczDzienPracy(Dzien dzien){
        int nadgodziny = dzien.iloscGodzin - (int)(Etat*8);
        if (nadgodziny<=0)
            return godzinowaStawka*dzien.iloscGodzin;
        else
            return godzinowaStawka*EtatGodzin + nadgodziny*godzinowaStawka*1.5;
    }

    public Robotnik(String nazwisko, double etat, Date zaczalPracowac, double godzinowaStawka) {
        super(nazwisko, etat, zaczalPracowac);
        this.godzinowaStawka = godzinowaStawka;
    }
}


