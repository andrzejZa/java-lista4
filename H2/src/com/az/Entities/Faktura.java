package com.az.Entities;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Set;

public class Faktura implements Comparable<Faktura>{
    private Hashtable<Towar, Integer> Towary;
    private Klient klient;
    private Integer Numer;
    private Date DataWystawienia;
    private Date DataSprzedazu;
    private String Platnosc;
    private Date TerminZaplaty;
    public Faktura( Klient klient, int numer){
        Towary = new Hashtable();
        this.klient = klient;
        Numer = numer;
        DataWystawienia = new Date();
        TerminZaplaty = new Date();
    }
    public void DodajTowar(Towar towar){
        if (Towary.containsKey(towar))
            Towary.put(towar, Towary.get(towar) + 1);
           else
               Towary.put(towar,1);
    }
    public void UsunTowar(Towar towar){
        if (Towary.containsKey(towar))
            Towary.remove(towar);
    }

    @Override
    public int compareTo(Faktura o) {
        return Numer.compareTo(o.Numer);
    }
    public double GetSum() {
        double result = 0;
        Set<Towar> keys = Towary.keySet();
        for (Towar key : keys) {
            result += key.Cena * Towary.get(key);
        }
        return result;
    }

    public String[] ToString(){
        return new String[]{Numer +"",klient.Nazwa,GetSum()+"", DataWystawienia.toString(),TerminZaplaty.toString()};
    }
    public void Wydrukuj(){
        //TODO
    }
    public Klient getKlient(){
        return klient;
    }
}
