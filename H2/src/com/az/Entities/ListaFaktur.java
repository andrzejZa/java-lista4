package com.az.Entities;

import com.az.TableBuilder;
import com.az.Utils.SimpleArrayList;

public class ListaFaktur {
    int startNumber = 1000;
    SimpleArrayList<Faktura> faktury;

    public ListaFaktur() {
        this.faktury = new SimpleArrayList<>();
    }

    public Faktura DodajFakture(Klient klient){
        Faktura f = new Faktura(klient,startNumber++);
        faktury.Add(f);
        return f;
    }

    public Faktura[] ZnajdzFaktury(Klient klient){
        SimpleArrayList<Faktura> fakturyKlienta = new SimpleArrayList<>();
        for (int i=0;i<faktury.size();i++){
            if (faktury.Get(i).getKlient().Nazwa.equalsIgnoreCase(klient.Nazwa))
                fakturyKlienta.Add(faktury.Get(i));
        }
        return fakturyKlienta.ToArray();
    }

    public Faktura ZnajdzFakture(int numer){
        return faktury.Find("Numer",numer);
    }

    public void DrukujFakture(int numer){
       Faktura f= faktury.Find("Numer",numer);
        f.Wydrukuj();
    }

    public void WydrukujListeFaktur(){
        DrukujListe(faktury.ToArray());
    }

    public void WydrukujListeFakturKlienta(Klient klient){
        DrukujListe(ZnajdzFaktury(klient));
    }

    private void DrukujListe(Faktura[] faktury){
        TableBuilder tb = new TableBuilder();
        tb.AddHeader(new String[]{"Numer","Klient","Suma","Data sprzedazu","Data platnosci"});
        for (int i=0;i<faktury.length;i++ ){
            tb.AddRow(faktury[i].ToString());
        }
        tb.Print();
    }


}
