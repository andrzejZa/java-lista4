package com.az.Entities;

public class Towar implements Comparable<Towar> {
    String Nazwa;

    public Towar(String nazwa, double cena) {
        Nazwa = nazwa;
        Cena = cena;
    }

    double Cena;

    @Override
    public int compareTo(Towar o) {

        return this.Nazwa.compareTo( o.Nazwa);
    }
}
