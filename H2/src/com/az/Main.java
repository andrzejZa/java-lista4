package com.az;

import com.az.Entities.Faktura;
import com.az.Entities.Klient;
import com.az.Entities.ListaFaktur;
import com.az.Entities.Towar;

public class Main {

    public static void main(String[] args) {
	// write your code here
        ListaFaktur lf = new ListaFaktur();
        Klient k = new Klient();
        k.Nazwa = "Politechnika Wroclawska";
        Faktura f = lf.DodajFakture(k);
        f.DodajTowar(new Towar("Komputer", 1000));
        f.DodajTowar(new Towar("Monitor", 500));
        f.Wydrukuj();
        lf.WydrukujListeFakturKlienta(k);
    }
}
