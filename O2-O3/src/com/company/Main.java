package com.company;


import com.az.TableBuilder;

import java.lang.reflect.Array;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Tablica t = new Tablica(10);

        printOutput("Elementy", t.GetElements());
        printOutput("Distinct", t.distinct());
        printOutput("NotUnique", t.notUnique());
        printOutput("Max/index", new Integer[]{t.getMax(), t.getMaxIndex()});
        printOutput("Sorted", t.SortAsc());
    }

    static void printOutput(String header, Integer[] elems){
        if (elems == null) {
            System.out.println(header + " no elements");
        return;
        }
        TableBuilder tb = new TableBuilder();
        tb.AddHeader(new String[]{header});
        for (int i=0; i< elems.length;i++)
            tb.AddRow(new String[]{elems[i].toString()});
        tb.Print();
        System.out.println();
    }
}

class Tablica {

    private SimpleArrayList<Integer> listElems;
    public Tablica(int elemCount){
         Integer[] elems;
        elems = new Integer[elemCount];
        for (int i=0;i<elemCount;i++){
            elems[i] = (int)(Math.random()*1000);
        }
        listElems = new SimpleArrayList<>();
        listElems.AddSome(elems);
    }

    public long getSum(){
        long sum =0;
        for (int i=0;i<listElems.size();i++){
           sum += listElems.Get(i);
        }
        return sum;
    }
    public int getMax(){
        return  listElems.Max();
    }

    public int getMaxIndex(){
       return listElems.IndexOf(listElems.Max());
    }

    public Integer[] distinct(){
       return  listElems.Distinct().ToArray();

    }

    public Integer[] notUnique(){

        return  listElems.NotUnique().ToArray();
    }

    public Integer[] SortAsc(){
        return listElems.SortAsc().ToArray();
    }
    public void removeValue(int value){
        listElems.RemoveAt(listElems.FirstIndexOf(value));
    }

    public boolean isElemsUnique(){

       for (int i=0; i< listElems.size();i++){
           if (i != listElems.FirstIndexOf(listElems.Get(i)))
               return false;
       }
       return true;
    }

   public Integer[] GetElements(){
        return (Integer[]) listElems.ToArray();
   }

}

class SimpleArrayList<T> {

    private int length;
    private Object[] storage;
    private final  int partLength = 100;
    public SimpleArrayList(){
        length =0;
        storage = new Object[partLength];
    }
    private SimpleArrayList(Object[] array){

        length =0;
        storage = new Object[partLength];
        for (int i=0 ;i<array.length ;i++)
            this.Add((T)array[i]);
    }

    public T Get(int index){
        return (T)storage[index];
    }

    public void Set(int index,T value){
        storage[index] = value;
    }

    public int FirstIndexOf(T value){
       boolean isComparable =  (storage[0] instanceof Comparable);
            for (int i = 0; i < length; i++) {
                if (isComparable?((Comparable)storage[i]).compareTo(value) == 0:storage[i] == value)
                    return i;
            }

        return -1;

    }

    public int IndexOf(T value){
        for (int i = 0; i < length; i++) {
                if (storage[i] == value)
                    return i;
        }
        return  -1;
    }

    public void RemoveAt(int index){
        Object[] newStorage = new Object[storage.length];
        boolean itemRemoved = false;
        for (int i=0;i< length;i++){
            if (i != index) {
                newStorage[i] = storage[i];
            } else {
                itemRemoved = true;
            }
        }
        if (itemRemoved) {
            storage = newStorage;
        length--;
        }
    }

    public void RemoveAll(T elem){
        boolean isComparable =(storage[0] instanceof Comparable );
        Object[] newStorage = new Object[storage.length];
        int itemRemoved = 0;

        for (int i=0;i< length;i++){
            if (!(isComparable?(((Comparable)storage[i]).compareTo(elem)==0):(storage[i] == elem))) {
                newStorage[i] = storage[i];
            } else {
                itemRemoved++;
            }
        }
        if (itemRemoved>0) {
            storage = newStorage;
            length-=itemRemoved;
        }
    }

    public T Max(){

        T max = (T)storage[0];
        if (!(storage[0] instanceof Comparable ))
            return max;
        for (int i=0;i<length;i++){
            if (((Comparable)max).compareTo(storage[i])<0)
                max = (T)storage[i];
        }
        return max;
    }

    public T Min(){
        T min = (T)storage[0];
        if (!(storage[0] instanceof Comparable ))
            return min;
        for (int i=0;i<length;i++){
            if (((Comparable)min).compareTo(storage[i])>0)
                min = (T)storage[i];
        }
        return min;
    }

    public  SimpleArrayList<T> SortAsc(){
        return Sort(true);
    }
    public  SimpleArrayList<T> SortDesc(){
        return Sort(false);
    }

    private SimpleArrayList<T> Sort(boolean asc){
        Object[] resultArr = new Object[length];
        for (int i=0;i< length;i++){
            resultArr[i] = storage[i];
        }
        if (storage[0] instanceof Comparable ){
        T temp ;


        for(int i=0; i < length; i++){
            for(int j=1; j < (length-i); j++){

                if(asc?((Comparable)resultArr[j-1]).compareTo(resultArr[j]) > 0:
                        ((Comparable)resultArr[j-1]).compareTo(resultArr[j]) < 0){
                    //swap the elements!
                    temp = (T)resultArr[j-1];
                    resultArr[j-1] = resultArr[j];
                    resultArr[j] = temp;
                }

            }
        }

        }
        return new SimpleArrayList<T>(resultArr);
    }

    public SimpleArrayList<T> Distinct(){
        SimpleArrayList<T> result = new SimpleArrayList();

        for (int i=0; i< length;i++){
            if (result.FirstIndexOf((T)storage[i]) == -1)
                result.Add((T)storage[i]);
        }
        return result;
    }

    public SimpleArrayList<T> NotUnique(){
        SimpleArrayList<T> list = new SimpleArrayList();
        SimpleArrayList<T> resultList = new SimpleArrayList();

        for (int i=0; i< length;i++){
            if (list.FirstIndexOf((T)storage[i]) == -1)
                list.Add((T)storage[i]);
            else resultList.Add((T)storage[i]);

        }
        return  resultList;
    }

    public int size(){
        return length;
    }

    public <T> T[] ToArray(){
        if (length == 0)
            return null;
        @SuppressWarnings("unchecked")
        T[] tempStorage = (T[])Array.newInstance(storage[0].getClass(),length);

        for (int i=0;i<length;i++){
            tempStorage[i] = (T)storage[i];
        }
        return  tempStorage;
    }

    public void Add (T item){
        if (length == storage.length){
            Object[] newStorage = new Object[storage.length + partLength];
            for (int i=0;i< storage.length;i++){
                newStorage[i] = storage[i];
            }
            storage = newStorage;
        }

        storage[length] = item;
        length++;
    }

    public void AddSome(T[] items)
    {
        for (int i=0;i< items.length;i++){
            this.Add(items[i]);
        }
    }


}