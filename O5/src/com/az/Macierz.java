package com.az;

public class Macierz implements Comparable<Macierz>{

    private int[][] storage;

    public Macierz(int wierszy, int columny){
        wierszy = wierszy>20?20:wierszy;
        columny = columny>30?30:columny;
        storage = new int[wierszy][columny];

        for (int i=0;i< storage.length;i++){
            for (int j=0;j<storage[i].length;j++){
                storage[i][j]=(int)(Math.random()*1000);
            }
        }
    }

    public Macierz(int[][] newStorage){
        storage = newStorage;
    }
    public int GetMax(){
        int max =0;
        for (int i=0;i< storage.length;i++){
            for (int j=0;j<storage[i].length;j++){
                if (max < storage[i][j])
                    max = storage[i][j];
            }
        }
        return max;
    }

    public Index GetMaxIndex(){
        int max =0;
        Index index = new Index(0,0);
        for (int i=0;i< storage.length;i++){
            for (int j=0;j<storage[i].length;j++){
                if (max < storage[i][j]) {
                    max = storage[i][j];
                    index= new Index(i,j);
                }
            }
        }
        return index;
    }
    public int RowMaxElementSum(){
        int row=0;
        int sum=0;
        for (int i=0;i< storage.length;i++){
            int rowSum =0;
            for (int j=0;j<storage[i].length;j++){
                rowSum+=storage[i][j];
            }
            if (rowSum>sum){
                sum = rowSum;
                row = i;
            }
        }
        return row;
    }
    public long Sum(){
        long sum=0;
        for (int i=0;i< storage.length;i++){

            for (int j=0;j<storage[i].length;j++){
                sum+=storage[i][j];
            }

        }
        return sum;
    }
    public boolean ElementExists(int x){
        return  ElementIndex(x) !=null;
    }
    public Index ElementIndex(int x){

        Index index = null;
        for (int i=0;i< storage.length;i++){
            for (int j=0;j<storage[i].length;j++){
                if (x == storage[i][j]) {

                    index= new Index(i,j);
                }
            }
        }
        return index;
    }
    public boolean IsUnique(){
        for (int i=0;i< storage.length;i++){
            for (int j=0;j<storage[i].length;j++){
                Index index = ElementIndex(storage[i][j]);
                if (index.getRow() !=i || index.GetColumn() !=j)
                    return false;

            }
        }
        return true;
    }

    public Macierz Transponowanie(){
        int[][] mT = new int[this.storage[0].length][this.storage.length];
        for (int i=0;i<storage[0].length;i++){
            for (int j=0;j<storage.length;j++){
                mT[i][j] = storage[j][i];
            }
        }
        return new Macierz(mT);
    }

    @Override
    public int compareTo(Macierz m) {
        return MacierzComparator.Get().Compare(this,m);

    }



    class Index{
        int Wiersz;
        int Columna;

        public Index(int wiersz, int columna) {
            Wiersz = wiersz;
            Columna = columna;
        }

        public int getRow(){
            return  Wiersz;
        }
        public int GetColumn(){
            return  Columna;
        }
        @Override
        public String toString() {
            return "Wiersz:"+Wiersz+" Columna:"+Columna;
        }
    }


}

