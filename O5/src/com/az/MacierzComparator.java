package com.az;

public class MacierzComparator implements Comparator
{
    private Comparator comparator;

    private static MacierzComparator instance;

    public static MacierzComparator Get(){
        if (instance == null) {
            instance = new MacierzComparator();
            instance.comparator = Comparators.CompareSum();
        }
        return instance;
    }
    public static MacierzComparator SetComparator(Comparator c){
        MacierzComparator current =  Get();
        if (c != null)
             current.comparator =c;
        return current;
    }


    @Override
    public int Compare(Macierz m1, Macierz m2) {
        MacierzComparator current =  Get();
        return current.Compare(m1,m2);
    }
}

class CompareSum implements Comparator{

    @Override
    public int Compare(Macierz m1, Macierz m2) {
        return (int)(m1.Sum()-m2.Sum());
    }
}

class CompareMaxElement implements Comparator{

    @Override
    public int Compare(Macierz m1, Macierz m2) {
        return m1.GetMax()-m2.GetMax();
    }
}

class CompareRowSum implements Comparator{

    @Override
    public int Compare(Macierz m1, Macierz m2) {
        return m1.RowMaxElementSum()-m2.RowMaxElementSum();
    }
}

interface Comparator{
    int Compare(Macierz m1, Macierz m2);
}



class Comparators{
    public static Comparator CompareSum(){
        return  new CompareSum();
    }

    public static Comparator CompareMaxElement(){
        return  new CompareMaxElement();
    }

    public static Comparator CompareRowSum(){
        return  new CompareRowSum();
    }
}