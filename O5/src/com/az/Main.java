package com.az;

import java.lang.reflect.Array;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Tablica Tab = new Tablica(10);

        System.out.println("suma wartości elementów macierzy "+Tab.getSum());
        System.out.println("wartość maksymalna w macierzy "+Tab.getMaxItem());
        System.out.println("Element maksymalny "+Tab.getMaxIndex().toString());
        System.out.println("wiersz o maksymalnej sumie wartości elementów "+Tab.getMaxIndex().toString());
        Macierz.Index elemIndex = Tab.FindElem(3);
        System.out.println("czy podana ( jako parametr ) wartość \"3\" występuje w macierzy "+
                (elemIndex ==null?"- nie": "- tak: "+elemIndex.toString()));
        System.out.println("czy wszystkie macierze  są różnowartościowe. "+(Tab.distinct()?" Tak":" Nie"));
        //TODO: StackOwerflow exceprion in
        // MacierzComparator.SetComparator(Comparators.CompareMaxElement());
        // return listElems.Max().GetMax();
    }
}

class Tablica {

    private SimpleArrayList<Macierz> listElems;
    public Tablica(int elemCount){
        Macierz[] elems;
        elems = new Macierz[elemCount];
        for (int i=0;i<elemCount;i++){
            elems[i] = new Macierz(20,30);
        }
        listElems = new SimpleArrayList<>();
        listElems.AddSome(elems);
    }
// suma wartości elementów macierzy,
    public long getSum(){
        long sum =0;
        for (int i=0;i<listElems.size();i++){
            sum += listElems.Get(i).Sum();
        }
        return sum;
    }

    //wartość maksymalna w macierzy
    public int getMaxItem(){
        int max =0;
        for (int i=0;i<listElems.size();i++){
            int maxItem = listElems.Get(i).GetMax();
            if (maxItem>max)
                max = maxItem;

        }
        return max;

        //albo
//        MacierzComparator.SetComparator(Comparators.CompareMaxElement());
//        return listElems.Max().GetMax();


    }

    //Element maksymalny (wynikiem powinien być obiekt klasy Indeks
    public Macierz.Index getMaxIndex(){
        Macierz.Index index = listElems.Get(0).GetMaxIndex();
        int max =0;
        for (int i=0;i<listElems.size();i++){
            int maxItem = listElems.Get(i).GetMax();
            if (maxItem>max) {
                max = maxItem;
                index = listElems.Get(i).GetMaxIndex();
            }

        }
        return index;
        //albo
//        MacierzComparator.SetComparator(Comparators.CompareMaxElement());
//        return listElems.Max().GetMaxIndex();
    }

    //wiersz o maksymalnej sumie wartości elementów
    public int getMaxWiersz(){
        int max =0;
        for (int i=0;i<listElems.size();i++){
            int maxWiersz = listElems.Get(i).RowMaxElementSum();
            if (maxWiersz>max)
                max = maxWiersz;

        }
        return max;

        //albo
//        MacierzComparator.SetComparator(Comparators.CompareRowSum());
//        return listElems.Max().RowMaxElementSum();
    }

    //  czy podana ( jako parametr ) wartość występuje w macierzy,
    public Macierz.Index FindElem(int x) {


        for (int i=0;i<listElems.size();i++){
            Macierz.Index index = listElems.Get(i).ElementIndex(x);
            if (index != null)
                return index;

        }
        return null;
    }

    //  czy macierz  jest różnowartościowa.
    public boolean distinct(){

        for (int i=0;i<listElems.size();i++) {
            if (!listElems.Get(i).IsUnique()) ;
            return false;
        }
        return true;

        }



    public Macierz[] GetElements(){
        return (Macierz[]) listElems.ToArray();
    }

}