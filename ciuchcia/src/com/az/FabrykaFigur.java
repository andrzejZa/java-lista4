package com.az;

class FabrykaFigur {
    final static int maksymalnyRozmiar = 10;
    static Figura DajLosowaFigure(){
        int randomizer = (int)(Math.random()*4);
        switch (randomizer){
            case 0:
                return new Kolo(Math.random()*maksymalnyRozmiar);

            case 1:
                return new trojkat(Math.random()*maksymalnyRozmiar, Math.random()*maksymalnyRozmiar,Math.random()*maksymalnyRozmiar);

            case 2:
                return  new prostokat(Math.random()*maksymalnyRozmiar,Math.random()*maksymalnyRozmiar);

            case 3:
                return new kwadrat(Math.random()*maksymalnyRozmiar);

        }
        return null;
    }
}


