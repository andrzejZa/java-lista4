package com.az;

public class FiguraZlozona {
    private Figura[] zamowienie;

    public  FiguraZlozona(int iloscFigur){
        zamowienie = new Figura[iloscFigur];
        for (int i=0;i<iloscFigur; i++)
        zamowienie[i] = FabrykaFigur.DajLosowaFigure();
    }

    double pole(){
        double result =0;
        for (int i=0;i< zamowienie.length;i++){

            result +=zamowienie[i].Pole();
        }
        return result;
    }
    void drukujZamovienie(){

        for (int i=0;i< zamowienie.length;i++){
          System.out.println(i+" "+ zamowienie[i].toString());

        }
    }

}
