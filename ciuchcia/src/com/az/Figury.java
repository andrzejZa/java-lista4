package com.az;


import java.text.DecimalFormat;

abstract class Figura {
    final DecimalFormat df = new DecimalFormat("#.##");
    abstract double Pole();
    public abstract String toString();
}

class Kolo extends Figura {
    private double promien;
    public Kolo(double promien){
        this.promien = promien;
    }

    @Override
    double Pole() {
        return Math.PI*promien*promien;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() +" Promien="+ df.format(promien)+". Pole="+df.format(Pole());
    }
}

class trojkat extends Figura{
    private double a;
    private double b;
    private double c;
    public trojkat(double a, double b, double c){
        this.a =a;
        this.b = b;
        this.c = c;
    }

    @Override
    double Pole() {
        double p =a+b+c;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));

    }
    @Override
    public String toString() {
        return this.getClass().getSimpleName().toString() +" Strona a="+ df.format(a)+" Strona b="+ df.format(b)+" Strona c="+ df.format(c)+". Pole="+df.format(Pole());
    }
}
class prostokat extends Figura{
     double a;
     double b;

    public prostokat(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    double Pole() {
        return a*b;
    }
    @Override
    public String toString() {
        return this.getClass().getSimpleName().toString() +" Strona a="+ df.format(a)+" Strona b="+ df.format(b)+". Pole="+df.format(Pole());
    }
}
class kwadrat extends prostokat{

    public kwadrat(double a) {
        super(a,a);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName().toString() +" Strona a="+df.format(super.a)+". Pole="+df.format(Pole());
}
}