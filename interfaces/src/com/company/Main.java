package com.company;

import com.az.TableBuilder;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Funkcja f1 = new FunkcjaLiniowa(2,4);

        Funkcja f2 = new FunkcjaKwadratowa();
        Tabela t = new Tabela(f1, 1,10,0.1);
        t.WyswietlTabele();


    }
}
interface Funkcja{
    public double f(double x);
    public String Wzor();
}

class FunkcjaLiniowa implements Funkcja{
    private  double a,b;

    public FunkcjaLiniowa(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double f(double x) {
        return a*x+b;
    }

    @Override
    public String Wzor() {
        return "f(x)="+a+"*x" +(b>=0?"+":"-")+b;
    }

}


class Tabela{
    private Funkcja func;
    private  double xp,xk,dx;

    public Tabela(Funkcja func, double xp, double xk, double dx) {
        this.func = func;
        this.xp = xp;
        this.xk = xk;
        this.dx = dx;
    }
    public void Wyswietl(){
        System.out.println(func.Wzor());
        System.out.println("X        Y");
        double x = xp;
        do {
            System.out.println(String.format("%8.3f",x)+String.format(" %14.5f",func.f(x)));
            x = x+dx;

        } while (x <xk);
        }

    public void WyswietlTabele(){
        TableBuilder tb = new TableBuilder();
        System.out.println(func.Wzor());
        tb.AddHeader(new String[] {" f(x) "," x "});
        double x = xp;
        do {
            tb.AddRow(new String[]{String.format("%.3f",x), String.format("%.5f",func.f(x))});

            x = x+dx;

        } while (x <xk);
        tb.Print();

    }
}

class FunkcjaKwadratowa implements  Funkcja{


    @Override
    public double f(double x) {
        return x*x;
    }

    @Override
    public String Wzor() {
        return "f(x)=x*x";
    }
}
